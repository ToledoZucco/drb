
%Frequency responses
%define a more precise freauency domain
omega_min_plot = 0.1*pi;
omega_max_plot = 30*pi;
omega_plot = logspace(log10(omega_min_plot),log10(omega_max_plot),400);

for j = 1:length(omega_plot)
    G_j      = G(1i*omega_plot(j));
    MAG_dB   = 20*log10(abs(G_j));
    PHA_RD   = angle(G_j);
    MAG_11(j)   = MAG_dB(1,1); 
    PHA_11(j)   = PHA_RD(1,1); 
    MAG_12(j)   = MAG_dB(1,2); 
    PHA_12(j)   = PHA_RD(1,2);
    MAG_22(j)   = MAG_dB(2,2); 
    PHA_22(j)   = PHA_RD(2,2); 
    
    G_r_j           = Gr(1i*omega_plot(j));
    MAG_r_dB   = 20*log10(abs(G_r_j));
    PHA_r_RD   = angle(G_r_j);
    MAG_r_11(j)   = MAG_r_dB(1,1); 
    PHA_r_11(j)   = PHA_r_RD(1,1); 
    MAG_r_12(j)   = MAG_r_dB(1,2); 
    PHA_r_12(j)   = PHA_r_RD(1,2);
    MAG_r_22(j)   = MAG_r_dB(2,2); 
    PHA_r_22(j)   = PHA_r_RD(2,2);
    
    G_r2_j           = Gr2(1i*omega_plot(j));
    MAG_r2_dB   = 20*log10(abs(G_r2_j));
    PHA_r2_RD   = angle(G_r2_j);
    MAG_r2_11(j)   = MAG_r2_dB(1,1); 
    PHA_r2_11(j)   = PHA_r2_RD(1,1); 
    MAG_r2_12(j)   = MAG_r2_dB(1,2); 
    PHA_r2_12(j)   = PHA_r2_RD(1,2);
    MAG_r2_22(j)   = MAG_r2_dB(2,2); 
    PHA_r2_22(j)   = PHA_r2_RD(2,2);

end

for j = 1:length(la_plot)
    G_la_j      = G(la_plot(j));
    MAG_dB   = 20*log10(abs(G_la_j));
    PHA_RD   = angle(G_la_j);
    MAG_la_11(j)   = MAG_dB(1,1); 
    PHA_la_11(j)   = PHA_RD(1,1); 
    MAG_la_12(j)   = MAG_dB(1,2); 
    PHA_la_12(j)   = PHA_RD(1,2);
    MAG_la_22(j)   = MAG_dB(2,2); 
    PHA_la_22(j)   = PHA_RD(2,2);
    
    G_mu_j      = G(mu_plot(j));
    MAG_dB   = 20*log10(abs(G_mu_j));
    PHA_RD   = angle(G_mu_j);
    MAG_mu_11(j)   = MAG_dB(1,1); 
    PHA_mu_11(j)   = PHA_RD(1,1); 
    MAG_mu_12(j)   = MAG_dB(1,2); 
    PHA_mu_12(j)   = PHA_RD(1,2);
    MAG_mu_22(j)   = MAG_dB(2,2); 
    PHA_mu_22(j)   = PHA_RD(2,2);
end
figure
set(gcf,'units','points','position',[x0screen,y0screen,WidthScreen,HeightScreen])
semilogx(omega_plot,MAG_22,'LineWidth',lw)
hold on
semilogx(omega_plot,MAG_r_22,'--','LineWidth',lw)
semilogx(omega_plot,MAG_r2_22,'--','LineWidth',lw,'Color',Green)
semilogx(imag(la_plot),MAG_la_22,'.','LineWidth',lw,'MarkerSize',ms)
semilogx(imag(mu_plot),MAG_mu_22,'.','LineWidth',lw,'MarkerSize',ms,'Color',Yellow)
xlim([0,max(omega_plot)])
set(gca,'FontSize',fst)
xline(omega_min,'LineWidth',lw)
xline(omega_max,'LineWidth',lw)
xlabel('$\omega$ $(rad/s)$','Interpreter','latex','FontSize',fs)
ylabel('Mag. $G ^{22}$ $(dB)$','Interpreter','latex','FontSize',fs)
ylim([-50,90])

leg2{1} = sprintf('Mag. $G^{22}(s)$ Discretized model');
leg2{2} = sprintf('Mag. $G_r^{22}(s)$ Preliminary ROM');
leg2{3} = sprintf('Mag. ${G}_r^{22}(s)$ Final ROM');
leg2{4} = sprintf('$\\lambda _i$');
leg2{5} = sprintf('$\\mu_i$');
legend(leg2,'Interpreter','latex','FontSize',fs,'Location','northwest')
Pos = get(gca,'Position');
set(gca,'Position',Pos+[xoff yoff Widthoff Heightoff])