# Discretization and Reduction of BC-PHSs

These Matlab codes provide the steps for the discretization using the Finite Element Method (FEM) and the Model Order Reduction (MOR) using the Loewner framerkor. These codes are quite general and can treat different
types of Partial Differential Equations (PDEs) with different types of Boundary Conditions (BC). We consider the following general class of PDEs so called Boundary-Controlled port-Hamiltonian Systems (BC-PHSs):

```math
 \dfrac{\partial x}{\partial t}(\zeta,t) = P\dfrac{\partial e}{\partial \zeta}(\zeta,t)-G e(\zeta,t), \quad  e = \mathcal{H}x(\zeta,t), \quad u(t) = V_\mathcal{B}\begin{pmatrix}e(b,t) \\ e(a,t) \end{pmatrix},
 \quad y(t) = V_\mathcal{B}\begin{pmatrix}e(b,t) \\ e(a,t) \end{pmatrix},
```

with $\zeta \in [a,b]$ the space, $t\geq 0$ the time, and $x(\zeta,t) \in L^2([a,b];\mathbb{R}^n)$ the state, $e(\zeta,t) \in H^1([a,b];\mathbb{R}^n)$ the co-energy state, $u(t)$ the boundary input,
$y(t)$ the boundary output, $P$, $G$, $\mathcal{H} \in \mathbb{R}^{n \times n}$ such that $P = P ^\top$ and invertible, $G + G^\top \geq 0 $, $H>0$, and $V_\mathcal{B}$, $V_\mathcal{C} \in \mathbb{R}^{n \times 2n}$. 

These codes provides a generique code for the numerical discretization and reduction of the previous BC-PHS. The codes 'PFEM.m' and 'MOR.m' are independent of the matrices defined previously. So, one can
change the PDEs and BC and these codes will provide a discretized model and a reduced order model related to the BC-PHSs. All the details are described in the following article:  

[Jesus-Pablo Toledo-Zucco, Denis Matignon, Charles Poussot-Vassal, Yann Le Gorrec, 2024](https://doi.org/10.48550/arXiv.2402.06425)

The following two examples uses the same codes 'PFEM.m' and 'MOR.m' to construct the discretized model and the reduced order model. The only differences are the definitions of the matrices of the BC-PHS.

## Example_A.m

This example provides a discretized model and a reduced order model for the 1D wave equation.

## Example_B.m

This example provides a discretized model and a reduced order model for the 1D Timoshenko beam.




