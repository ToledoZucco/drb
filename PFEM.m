
P11 = P(1:n1,1:n1);
P12 = P(1:n1,n1+1:n1+n2);
P21 = P(n1+1:n1+n2,1:n1);
P22 = P(n1+1:n1+n2,n1+1:n1+n2);

G11 = G(1:n1,1:n1);
G12 = G(1:n1,n1+1:n1+n2);
G21 = G(n1+1:n1+n2,1:n1);
G22 = G(n1+1:n1+n2,n1+1:n1+n2);


H1 = H(1:n1,1:n1);
H2 = H(n1+1:n1+n2,n1+1:n1+n2);

%Discretization
%For simplicity we keep N1 = N2 = N. The general case has not been coded.
% N        = 800;
N1       = N; 
N2       = N; 
h1       = (b-a)/(N1-1);  %Spatial step size
h2       = (b-a)/(N2-1);  %Spatial step size
zeta1    = a:h1:b;        %Spatial discretization
zeta2    = a:h2:b;        %Spatial discretization
zeta     = zeta1;
%Computing the matrices of the ODE
%Mass matrices
E1 = MassMatrix(a,b,N1);
for i = 1:n1-1
    E1 = blkdiag(E1,MassMatrix(a,b,N1));
end
E2 = MassMatrix(a,b,N2);
for i = 1:n2-1
    E2 = blkdiag(E2,MassMatrix(a,b,N2));
end
%Energy matrices. For the case of nonconstant values, one has to provide
%the integral up to the third of the function H_1(zeta) and H_1(zeta).
%Q1
for i=1:n1
    for j = 1:n1
        Integral1    = @(zeta) H1(i,j)*zeta;
        Integral2    = @(zeta) H1(i,j)*zeta^2/2;
        Integral3    = @(zeta) H1(i,j)*zeta^3/6;
        Q1{i,j}    = EnergyMatrix(a,b,N1,Integral1,Integral2,Integral3);
    end
end
Q1 = cell2mat(Q1);
%Q2      
for i=1:n2
for j = 1:n2
        Integral1    = @(zeta) H2(i,j)*zeta;
        Integral2    = @(zeta) H2(i,j)*zeta^2/2;
        Integral3    = @(zeta) H2(i,j)*zeta^3/6;
        Q2{i,j}    = EnergyMatrix(a,b,N2,Integral1,Integral2,Integral3);
end
end
Q2 = cell2mat(Q2);
%D^P-Matrices
%D_11^P
for i = 1:n1
    for j = 1:n1
        D11_P{i,j} = P11(i,j)*InterconnectionMatrix(N1)';
    end
end
D11_P = cell2mat(D11_P);
%D_12^P
for i = 1:n1
    for j = 1:n2
        D12_P{i,j} = P12(i,j)*InterconnectionMatrix(N1)';
    end
end
D12_P = cell2mat(D12_P);
%D_21^P
for i = 1:n2
    for j = 1:n1
        D21_P{i,j} = P21(i,j)*InterconnectionMatrix(N1)';
    end
end
D21_P = cell2mat(D21_P);
%D_22^P
for i = 1:n2
    for j = 1:n2
        D22_P{i,j} = P22(i,j)*InterconnectionMatrix(N2)';
    end
end
D22_P = cell2mat(D22_P);
D_P = [D11_P,D12_P;D21_P,D22_P];
%D^G-Matrices
%D_11^G
for i = 1:n1
    for j = 1:n1
        D11_G{i,j} = G11(i,j)*MassMatrix(a,b,N1);
    end
end
D11_G = cell2mat(D11_G);
%D_12^G
for i = 1:n1
    for j = 1:n2
        D12_G{i,j} = G12(i,j)*MassMatrix(a,b,N1);
    end
end
D12_G = cell2mat(D12_G);
%D_21^G
for i = 1:n2
    for j = 1:n1
        D21_G{i,j} = G21(i,j)*MassMatrix(a,b,N1);
    end
end
D21_G = cell2mat(D21_G);
%D_22^G
for i = 1:n2
    for j = 1:n2
        D22_G{i,j} = G22(i,j)*MassMatrix(a,b,N2);
    end
end
D22_G = cell2mat(D22_G);
D_G = [D11_G,D12_G;D21_G,D22_G];
%Boundary matrices
phi1_a = InputMatrixLeft(N1);
phi1_b = InputMatrixRight(N1);
phi2_a = InputMatrixLeft(N2);
phi2_b = InputMatrixRight(N2);
Phi1_b = phi1_b;
Phi1_a = phi1_a;
for i=2:n1
    Phi1_b = blkdiag(Phi1_b,phi1_b);
    Phi1_a = blkdiag(Phi1_a,phi1_a);
end
Phi2_b = phi2_b;
Phi2_a = phi2_a;
for i=2:n2
    Phi2_b = blkdiag(Phi2_b,phi2_b);
    Phi2_a = blkdiag(Phi2_a,phi2_a);
end
Omega_ba = [blkdiag(Phi1_b,Phi2_b),blkdiag(Phi1_a,Phi2_a)];

%Discretized model
E = blkdiag(E1,E2);
Q = blkdiag(Q1,Q2);
J = D_P - 1/2*(D_G - D_G') - Omega_ba*VC'*VB*Omega_ba';
R = 1/2*(D_G + D_G');
B = Omega_ba*VC'; 
S = E\Q;
A = (J-R)*S;
C = B'*S;
%If boundary dissipation is incorporated, one can add the following code
% K = diag([0.1,0.1]);        %Boundary dissipation
% K = sparse(K);
% A = A-B*K*C;

%The following matrix conditions are satisfy
D12_P + D21_P' - (Phi1_b*P12*Phi2_b'-Phi1_a*P12*Phi2_a')
D11_P + D11_P' - (Phi1_b*P11*Phi1_b'-Phi1_a*P11*Phi1_a')
D22_P + D22_P' - (Phi2_b*P22*Phi2_b'-Phi2_a*P22*Phi2_a')
D11_G+D11_G'
D22_G+D22_G'