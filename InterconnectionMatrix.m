%This function returns a sparse matrix generated integrating the test functions (hat functions)
%over the interval [a,b] with N elements.
%D = int(dPhi/dzeta*Phi^\top)
function D = InterconnectionMatrix(N)

%% D matrix
id = (1:N)';
jd = (1:N)';
dd = [-1/2;zeros(N-2,1);1/2];

isd = (1:N-1)';
jsd = (2:N)';
dsd = -1/2*ones(N-1,1);

iid = (2:N)';
jid = (1:N-1)';
did = 1/2*ones(N-1,1);

im = [id;isd;iid];
jm = [jd;jsd;jid];
dm = [dd;dsd;did];


D1 = sparse(im,jm,dm,N,N);
D = D1;
end
