
%Transfer function of the Discretized Model
G = @(s) C*((s*E-A)\B);

%4.1 Standard Loewner approach
%Definition of the right and left data
la = 1i*omega(1:2:2*Mr-1).';
mu = 1i*omega(2:2:2*Mr);

la_plot = la;   %Save the right data for ploting after
mu_plot = mu;   %Save the left data for ploting after

%The conjugated values have to be incorporated for a real realization
la_ = conj(la);
mu_ = conj(mu);

%The complet right and left data are organized in the following way
la = reshape([la; la_],[],1).';
mu = reshape([mu.';mu_.'],[],1);

%Create the tangential directions
In = eye(n);
for i = 1:n
    e{i} = In(:,i);
end

ri_0 = ones(1,n);
ri   = ri_0;
for i = 2:n
    ri = blkdiag(ri,ri_0);
end
rr = ri;
ll = ri';
for i = 2:Mr/n
    rr  = [rr,ri];
    ll  = [ll;ri'];
end
rr = rr(:,1:Nr);
ll = ll(1:Nr,:);
for i = 1:Nr
    r{:,i} = rr(:,i);
    l{i,:} = ll(i,:);
end
% i = 1;
% haut = 1;
% for j = 1:Mr
%     if haut
%         r{:,i}  = e1;
%         l{i,:}  = e1';
%         r{:,i+1}  = e1;
%         l{i+1,:}  = e1';
%         haut = 0;
%     else
%         r{:,i}  = e2;
%         l{i,:}  = e2';
%         r{:,i+1}  = e2;
%         l{i+1,:}  = e2';
%         haut = 1;
%     end
%     i  = i + 2;
% end
%Evaluate the right and left data 
for i = 1:2*Mr
    W{:,i} = G(la(i))*r{i};
end
for i = 1:2*Mr
    V{i,:} = l{i}*G(mu(i));
end
%Create the loewner and shifted Loewner matrices
for i = 1:2*Mr
    for j = 1:2*Mr
        LL{i,j} = (V{i}*r{j}-l{i}*W{j})/(mu(i)-la(j));
        LLs{i,j} = (mu(i)*V{i}*r{j}-la(j)*l{i}*W{j})/(mu(i)-la(j));
    end
end
LL = cell2mat(LL);
LLs = cell2mat(LLs);
W = cell2mat(W);
V = cell2mat(V);
La = diag(la);
Mu = diag(mu);
%Compute the tangential generalized controllability and observability
%matrices
for ii = 1:2*Mr
    Cb{:,ii} = (la(ii)*E-A)\B*r{ii};
    Ob{ii,:} = l{ii}*C*((mu(ii)*E-A)\speye(n*N));
end
Cb = cell2mat(Cb);
Ob = cell2mat(Ob);
% Check rank conditions for: s = \lambda \cup \mu
s = [la,mu.'];
for i = 1:length(s)
    [rank(s(i)*LL-LLs),rank([LL,LLs]),rank([LL;LLs])]
end
%chedk that det(s(i)*LL-LLs) is different to zero
for i = 1:length(s)
    det(s(i)*LL-LLs)
end
%Select the size of the reduced realization
rN = rank([LL,LLs]);
vN = rank(LL);
%Define the Loewner realization
El = -LL;  
Al = -LLs; 
Bl = V;    
Cl = W;
%This conditions are verified
Bl - Ob*B
Cl - C*Cb 
Ob*E*Cb + LL
Ob*A*Cb + LLs

%4.2. Transformation to a realization with real matrices
J_i = 1/sqrt(2)*[1,-1i;1,1i];
Jk = J_i;
for i = 2:Mr
    Jk = blkdiag(Jk,J_i);
end
Elb = Jk'*El*Jk;     Elb = real(Elb);
Alb = Jk'*Al*Jk;     Alb = real(Alb); 
Blb = Jk'*Bl;        Blb = real(Blb);   
Clb = Cl*Jk;         Clb = real(Clb);    
LL_lb   = -Elb;
LLs_lb  = -Alb;
V_lb    = Blb;
W_lb    = Clb;

%4.3Singular value decomposition
[Y,Sl,Xt] = svd([Elb,Alb],'econ');
[Yt,Sr,X] = svd([Elb;Alb],'econ');
Y = Y(:,1:rN);
X = X(:,1:rN);
Er = -Y'*LL_lb*X;    
Ar = -Y'*LLs_lb*X;   
Br = Y'*V_lb;        
Cr = W_lb*X;         

Gr = @(s) Cr*((s*Er-Ar)\Br);

%One can verify the interpolation conditions
for i = 1:length(la)
   l{i}*G(mu(i)) - l{i}*Gr(mu(i))
   G(la(i))*r{i} - Gr(la(i))*r{i}
end
T = real(Cb*Jk)*X;

%4.4 Second Iteration of the Loewner approach
Dr = 1*eye(n);
%Computing the Spectral zeros
ASZ = [sparse(rN,rN),Ar,Br;
       Ar', sparse(rN,rN), Cr';
       Br', Cr, Dr+Dr'];
BSZ = [sparse(rN,rN),Er,Br*0;
       -Er', sparse(rN,rN), Cr'*0;
        Br'*0,Cr*0,sparse(n,n)];

[VSZ,DSZ] = eig(full(ASZ),full(BSZ));
sSZ = diag(DSZ);
rSZ = VSZ(2*rN+1:end,:);
%Select the spectral zeros that are nonzero
indZS_all_positive = find(real(sSZ)>0 & real(sSZ)<Inf);
sSZ_all_positive = sSZ(indZS_all_positive);
rSZ_all_positive = rSZ(:,indZS_all_positive);

indZS_complex = find(real(sSZ)>0 & real(sSZ)<Inf & abs(imag(sSZ))< inf & abs(imag(sSZ))> 0.000);
sSZ_complex = sSZ(indZS_complex);
rSZ_complex = rSZ(:,indZS_complex);

indZS_real = find(real(sSZ)>0 & real(sSZ)<Inf & abs(imag(sSZ)) == 0);
sSZ_real = sSZ(indZS_real);
rSZ_real = rSZ(:,indZS_real);

sSZ_selected = [sSZ_complex;sSZ_real];

%Interpolation at Spectral zeros
la = sSZ_complex;
mu = -conj(la).';
r = rSZ_complex;
l = r';
M = length(la)/2;

la_real = sSZ_real;
mu_real = -la_real';

r_real = rSZ_real;
l_real = r_real';

la_s = [la;la_real];
mu_s = [mu,mu_real];

r_s = [r,r_real];
l_s = [l;l_real];
clear r l
for i = 1:2*M
    r{:,i} = r_s(:,i);
    l{i,:} = l_s(i,:);
end

Mr = M;
clear W V
for i = 1:2*Mr
    W{:,i} = G(la(i))*r{i};
end
for i = 1:2*Mr
    V{i,:} = l{i}*G(mu(i));
end

%Create the loewner matrix
clear LL LLs
for i = 1:2*Mr
    for j = 1:2*Mr
        LL{i,j} = (V{i}*r{j}-l{i}*W{j})/(mu(i)-la(j));
        LLs{i,j} = (mu(i)*V{i}*r{j}-la(j)*l{i}*W{j})/(mu(i)-la(j));
    end
end
LL = cell2mat(LL);
LLs = cell2mat(LLs);
W = cell2mat(W);
V = cell2mat(V);
La = diag(la);
Mu = diag(mu);

% Compute Loewner projectors
clear Cb Ob
for ii = 1:2*Mr
    Cb{:,ii} = (la(ii)*E-A)\B*r{ii};
    Ob{ii,:} = l{ii}*C*((mu(ii)*E-A)\speye(n*N));
end

Cb = cell2mat(Cb);
Ob = cell2mat(Ob);

r = cell2mat(r);
Dr = Dr*0;
El = -LL;  
Al = -LLs + r'*Dr*r; 
Bl = V - r'*Dr;    
Cl = W + Dr*r;
Dl = Dr*0;

%4.2 Obtaining a realization with real entries
J_i = 1/sqrt(2)*[1,-1i;1,1i];
Jk = J_i;
for i = 2:Mr
    Jk = blkdiag(Jk,J_i);
end
Elb = Jk'*El*Jk;     Elb = real(Elb);
Alb = Jk'*Al*Jk;     Alb = real(Alb); 
Blb = Jk'*Bl;        Blb = real(Blb);   
Clb = Cl*Jk;         Clb = real(Clb);    
Dlb = Dl;

LL_lb   = -Elb;
LLs_lb  = -Alb;
V_lb    = Blb;
W_lb    = Clb;

T2 = real(Cb*Jk);

Gr2 = @(s) Clb*((s*Elb-Alb)\Blb);

