% Plot Parameters
lw = 5;
ms = 30;
fs  = 20;
fst = fs*0.8;
x0screen=100;y0screen=40;WidthScreen=1200;HeightScreen=600;
xoff = -0.045; yoff = 0.010; Widthoff = 0.13; Heightoff = 0.0500;
Blue = [0 0.4470 0.7410];
Orange = [0.8500 0.3250 0.0980];
Yellow = [0.9290 0.6940 0.1250];
Violet = [0.4940 0.1840 0.5560];
Green = [0.4660 0.6740 0.1880];
Cyan = [0.3010 0.7450 0.9330];
quality = '-r300';