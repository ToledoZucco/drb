close all
clear all
clc

%% Define the PDE Matrices and parameters
% dx/dt = P*de/dz - G*e
%spatial domain
a = 0; b = 1;
H1 = 1;     %Youngs modulus
H2 = 1;     %the inverse of the mass density
P = [0,1;1,0];      P = sparse(P);
G = [0,0;0,0];      G = sparse(G);
H = [H1,0;0,H2];    H = sparse(H);  %For simplicity we consider H constant

%Boundary inputs and outputs
%Neuman BC
% VB = [0,0,1,0;
%       1,0,0,0];
% VC = [0,0,0,-1;
%       0,1,0,0];
%Mixed BC
VB = [0,0,0,1;
      1,0,0,0];
VC = [0,0,-1,0;
      0,1,0,0];
  
VB = sparse(VB);
VC = sparse(VC);

%One can verify that Jt is skew-symmetric
Jt = 1/2*blkdiag(P,-P) - VC'*VB;



%% Partitioned Finite Element
n1 = 1;         %number of states of the Partition 1 
n2 = 1;         %number of states of the Partition 2 
n  = n1 + n2;   %Number of states of the PDE
N        = 200;

PFEM


%% MOR using the Loewner framwork
% Standard Loewner
%Choose the number of interpolation points
Mr = 6;
Nr = 2*Mr;
%Choose the desired freauency of interpolation
omega_min = 0.9; omega_max = 9.05;
%It can be distributed in a linear space
omega = linspace(omega_min,omega_max,2*Mr).';
%or in a logarithmic scale
% omega = logspace(log10(omega_min),log10(omega_max),2*Mr).';
MOR

%% Figures: Freauency responses and numerical simulations
plotparameters
%Frequency responses 
FreqResponse


% Time Simulation
%time
Tfinal = 10; dt = 0.0001;
t = 0:dt:Tfinal; w0 = 1.2*1;
%Input signals
u = [sin(w0*t)*0;sin(w0*t)].*(1-heaviside(t-2*pi/w0*2)); 
u = [sin(w0*t)*0;sin(w0*t)].*(1-heaviside(t-5));

SYS = dss(A,B,C,0,E);
x0 = zeros(length(E),1);
[y,t,x] = lsim(SYS,u,t,x0); 
t = t';
y = y(:,:,1)';
x = x(:,:,1)';

SYSr = dss(Ar,Br,Cr,0,Er);
xr0 = zeros(length(Er),1);
[yr,tr,xr] = lsim(SYSr,u,t,xr0);
tr = tr';
yr = yr(:,:,1)';
xr = xr(:,:,1).';


SYSlb = dss(Alb,Blb,Clb,Dlb,Elb);
xlb0 = zeros(length(Elb),1);
[ylb,tlb,xlb] = lsim(SYSlb,u,t,xlb0);
tlb = tlb';
ylb = ylb(:,:,1)';
xlb = xlb(:,:,1).';

% Energies
H   = zeros(1,length(t));
Hr  = zeros(1,length(t));
Hlb = zeros(1,length(t));
for i = 1:length(t)
    
    xd  = x(:,i);
    xrd = T*xr(:,i);
    xlbd = T2*xlb(:,i);
    
    H(i)   = 1/2*xd'*Q*xd;
    Hr(i)  = 1/2*xrd'*Q*xrd;
    Hlb(i) = 1/2*xlbd'*Q*xlbd;
end

figure
hold on
set(gcf,'units','points','position',[x0screen,y0screen,WidthScreen,HeightScreen])
plot(t,H,'LineWidth',lw,'MarkerSize',ms)
plot(t,Hr,'LineWidth',lw,'MarkerSize',ms)
plot(t,Hlb,'--','LineWidth',lw,'MarkerSize',ms,'Color',Green)
ylim([0,10])
set(gca,'FontSize',fst)
xlabel('time $(s)$','Interpreter','latex','FontSize',fs)
ylabel('Hamiltonians','Interpreter','latex','FontSize',fs)
leg2{1} = sprintf('$H(t)$ Discretized model');
leg2{2} = sprintf('$H(t)$ Preliminary ROM');
leg2{3} = sprintf('$H(t)$ Final ROM');
legend(leg2,'Interpreter','latex','FontSize',fs,'Location','northwest')
Pos = get(gca,'Position');
set(gca,'Position',Pos+[xoff yoff Widthoff Heightoff])


xd  = x;
xrd = T*xr;
xlbd = T2*xlb;

x1d = xd(1:n1*N,:);
x2d = xd(n1*N+1:end,:);

x1rd = xrd(1:n1*N,:);
x2rd = xrd(n1*N+1:end,:);

x1lbd = xlbd(1:n1*N,:);
x2lbd = xlbd(n1*N+1:end,:);



Tw = h1*[zeros(1,N);[1/2*ones(N-1,1),tril(ones(N-1))-diag(1/2*ones(N-1,1))]];


wd            = Tw*x1d;
wrd           = Tw*x1rd;
wlbd          = Tw*x1lbd;


figure
set(gcf,'units','points','position',[x0screen,y0screen,WidthScreen,HeightScreen])
hold on
p1   = plot(zeta,wd(:,1),'LineWidth',lw);
p1r  = plot(zeta,wrd(:,1),'--','LineWidth',lw,'Color',Orange);
p1lb = plot(zeta,wlbd(:,1),'--','LineWidth',lw,'Color',Green);
set(gca,'FontSize',fst)
legSim{1} = sprintf('$w(\\zeta,t)$ PFEM');
legSim{2} = sprintf('$w(\\zeta,t)$ Preliminary ROM');
legSim{3} = sprintf('$w(\\zeta,t)$ FInal ROM');
legend(legSim,'Interpreter','latex','FontSize',fs,'Location','northwest')
title('Deformation','Interpreter','latex','FontSize',fs)
xlabel('space $(m)$','Interpreter','latex','FontSize',fs)
xlim([a,b])
% ylim([-ymax,ymax])
ylim([-10,10])
grid on

Nfigs = 500;
for j = 1:ceil(length(t)/Nfigs):length(t)

    set(p1,'YData',x2d(:,j))
    set(p1r,'YData',x2rd(:,j))
    set(p1lb,'YData',x2lbd(:,j))

    pause(0.01)
end



