%This function returns a sparse matrix generated integrating the test functions (hat functions)
%over the interval [a,b] with N elements.
%Q = int(H*Phi*Phi^\top)
%F = int(H)
%G = int(F)
%K = int(G)
function Q = EnergyMatrix(a,b,N,F,G,K)

%% Q matrix

h = (b-a)/(N-1);
zeta = a:h:b;

%Q1
id = (1:N)';
jd = (1:N)';
md = zeros(N,1);
md(1) = 1/h^2*((F(zeta(2))*(zeta(2)-zeta(2))^2 - 2*G(zeta(2))*(zeta(2)-zeta(2)) + 2*K(zeta(2)))...
              -(F(zeta(1))*(zeta(1)-zeta(2))^2 - 2*G(zeta(1))*(zeta(1)-zeta(2)) + 2*K(zeta(1))));
for i = 2:N-1
    md1 = 1/h^2*((F(zeta(i))*(zeta(i)-zeta(i-1))^2 - 2*G(zeta(i))*(zeta(i)-zeta(i-1)) + 2*K(zeta(i)))...
                -(F(zeta(i-1))*(zeta(i-1)-zeta(i-1))^2 - 2*G(zeta(i-1))*(zeta(i-1)-zeta(i-1)) + 2*K(zeta(i-1))));
    md2 = 1/h^2*((F(zeta(i+1))*(zeta(i+1)-zeta(i+1))^2 - 2*G(zeta(i+1))*(zeta(i+1)-zeta(i+1)) + 2*K(zeta(i+1)))...
                -(F(zeta(i))*(zeta(i)-zeta(i+1))^2 - 2*G(zeta(i))*(zeta(i)-zeta(i+1)) + 2*K(zeta(i))));
    md(i) = md1 + md2;
end
md(N) = 1/h^2*((F(zeta(N))*(zeta(N)-zeta(N-1))^2 - 2*G(zeta(N))*(zeta(N)-zeta(N-1)) + 2*K(zeta(N)))...
              -(F(zeta(N-1))*(zeta(N-1)-zeta(N-1))^2 - 2*G(zeta(N-1))*(zeta(N-1)-zeta(N-1)) + 2*K(zeta(N-1))));
% md = [h/3;2*h/3*ones(N-2,1);h/3];

isd = (1:N-1)';
jsd = (2:N)';
msd = zeros(N-1,1);
for i = 1:N-1
    msd(i) = -1/h^2*((F(zeta(i+1))*(zeta(i+1)-zeta(i))*(zeta(i+1)-zeta(i+1)) - G(zeta(i+1))*(2*zeta(i+1)-zeta(i)-zeta(i+1)) + 2*K(zeta(i+1)))...
                    -(F(zeta(i))*(zeta(i)-zeta(i))*(zeta(i)-zeta(i+1)) - G(zeta(i))*(2*zeta(i)-zeta(i)-zeta(i+1)) + 2*K(zeta(i))));
end

% msd = h/6*ones(N-1,1);

iid = (2:N)';
jid = (1:N-1)';
mid = msd;
% mid = h/6*ones(N-1,1);

im = [id;isd;iid];
jm = [jd;jsd;jid];
mm = [md;msd;mid];

Q = sparse(im,jm,mm,N,N);
end
