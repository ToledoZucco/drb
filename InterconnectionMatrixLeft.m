%This function returns a sparse matrix generated integrating the test functions (hat functions)
%over the interval [a,b] with N elements.
%D = Phi(a)*Phi(a)^\top

function D = InterconnectionMatrixLeft(N)
D = sparse(1,1,1,N,N);
end