%This function returns a sparse matrix generated integrating the test functions (hat functions)
%over the interval [a,b] with N elements.
%B = Phi(a)

function B = InputMatrixLeft(N)
B = sparse(1,1,1,N,1);
end
