function [Ad,Bd,Cd] = MidPoint(A,B,C,E,dt)

invA = (E-dt/2*A)\speye(length(A),length(A));
Ad = invA*(E+dt/2*A);
Bd = invA*dt*B;
Cd = C;
end
