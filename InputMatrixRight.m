%This function returns a sparse matrix generated integrating the test functions (hat functions)
%over the interval [a,b] with N elements.
%B = Phi(b)

function B = InputMatrixRight(N)
B = sparse(N,1,1,N,1);
end
