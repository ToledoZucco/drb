close all
clear all
clc

%% Define the PDE Matrices and parameters
% dx/dt = P*de/dz - G*e
%spatial domain
%Vibrating string parameters
a       = 0;      %Left position
b       = 1;      %Right position
g1      = 0;      %Internal Dissipation
g2      = 0;      %Internal Dissipation
K       = 1;      %Shear Modulus of elasticity
EI      = 1;      %Young Modulus of elasticity by moment of inertia
rho     = 1;      %Mass density
Irho    = 1;      %Rotatory momenent of inertia

%System structure
P  = [0,0,1,0;
      0,0,0,1;
      1,0,0,0;
      0,1,0,0];             %Differential operator matrix

P0 = [0,0,0,-1;
      0,0,0,0;
      0,0,0,0;
      1,0,0,0];             %Differential operator matrix
G0 = diag([0,0,g1,g2]);     %Dissipative term of the differential operator
G  = G0-P0;
H1 = K;
H2 = EI;
H3 = 1/rho;
H4 = 1/Irho;
H = blkdiag(blkdiag(H1,H2),blkdiag(H3,H4));
VB = [0,0,0,0,0,0,1,0;
      0,0,0,0,0,0,0,1;
      1,0,0,0,0,0,0,0;
      0,1,0,0,0,0,0,0];     %Input Matrix
VC = [0,0,0,0,-1,0,0,0;
      0,0,0,0,0,-1,0,0;
      0,0,1,0,0,0,0,0;
      0,0,0,1,0,0,0,0];     %Input Matrix

P = sparse(P);
G = sparse(G);
VB = sparse(VB);
VC = sparse(VC);

%One can verify that Jt is skew-symmetric
Jt = 1/2*blkdiag(P,-P) - VC'*VB;



%% Partitioned Finite Element
n1 = 2;         %number of states of the Partition 1 
n2 = 2;         %number of states of the Partition 2 
n  = n1 + n2;   %Number of states of the PDE
N        = 20;

PFEM


%% MOR using the Loewner framwork
% Standard Loewner
%Choose the number of interpolation points
Mr = 20;
Nr = 2*Mr;
%Choose the desired freauency of interpolation
omega_min = 0.9; omega_max = 9.05;
%It can be distributed in a linear space
omega = linspace(omega_min,omega_max,2*Mr).';
%or in a logarithmic scale
% omega = logspace(log10(omega_min),log10(omega_max),2*Mr).';
MOR

%% Figures: Freauency responses and numerical simulations

plotparameters
%Frequency responses 
FreqResponse


% Time Simulation

%time
Tfinal = 10; dt = 0.0001;
t = 0:dt:Tfinal; w0 = 1.2*1;
%Input signals
u = [sin(w0*t)*0;sin(w0*t)].*(1-heaviside(t-2*pi/w0*2)); 
u = [sin(w0*t)*0;sin(w0*t)].*(1-heaviside(t-5));
u = In(:,end)*(sin(w0*t).*(1-heaviside(t-5)));

SYS = dss(A,B,C,0,E);
x0 = zeros(length(E),1);
[y,t,x] = lsim(SYS,u,t,x0); 
t = t';
y = y(:,:,1)';
x = x(:,:,1)';

SYSr = dss(Ar,Br,Cr,0,Er);
xr0 = zeros(length(Er),1);
[yr,tr,xr] = lsim(SYSr,u,t,xr0);
tr = tr';
yr = yr(:,:,1)';
xr = xr(:,:,1).';


SYSlb = dss(Alb,Blb,Clb,Dlb,Elb);
xlb0 = zeros(length(Elb),1);
[ylb,tlb,xlb] = lsim(SYSlb,u,t,xlb0);
tlb = tlb';
ylb = ylb(:,:,1)';
xlb = xlb(:,:,1).';

% Energies
H   = zeros(1,length(t));
Hr  = zeros(1,length(t));
Hlb = zeros(1,length(t));
for i = 1:length(t)
    
    xd  = x(:,i);
    xrd = T*xr(:,i);
    xlbd = T2*xlb(:,i);
    
    H(i)   = 1/2*xd'*Q*xd;
    Hr(i)  = 1/2*xrd'*Q*xrd;
    Hlb(i) = 1/2*xlbd'*Q*xlbd;
end

figure
hold on
set(gcf,'units','points','position',[x0screen,y0screen,WidthScreen,HeightScreen])
plot(t,H,'LineWidth',lw,'MarkerSize',ms)
plot(t,Hr,'LineWidth',lw,'MarkerSize',ms)
plot(t,Hlb,'--','LineWidth',lw,'MarkerSize',ms,'Color',Green)
ylim([0,10])
set(gca,'FontSize',fst)
xlabel('time $(s)$','Interpreter','latex','FontSize',fs)
ylabel('Hamiltonians','Interpreter','latex','FontSize',fs)
leg2{1} = sprintf('$H(t)$ Discretized model');
leg2{2} = sprintf('$H(t)$ Preliminary ROM');
leg2{3} = sprintf('$H(t)$ Final ROM');
legend(leg2,'Interpreter','latex','FontSize',fs,'Location','northwest')
Pos = get(gca,'Position');
set(gca,'Position',Pos+[xoff yoff Widthoff Heightoff])


xd  = x;
xrd = T*xr;
xlbd = T2*xlb;

x1d = xd(1:n1*N,:);
x2d = xd(n1*N+1:end,:);

x1rd = xrd(1:n1*N,:);
x2rd = xrd(n1*N+1:end,:);

x1lbd = xlbd(1:n1*N,:);
x2lbd = xlbd(n1*N+1:end,:);


%Substates
x11d = x1d(1:N,:);
x12d = x1d(N+1:end,:);
x21d = x2d(1:N,:);
x22d = x2d(N+1:end,:);

x11rd = x1rd(1:N,:);
x12rd = x1rd(N+1:end,:);
x21rd = x2rd(1:N,:);
x22rd = x2rd(N+1:end,:);

x11lbd = x1lbd(1:N,:);
x12lbd = x1lbd(N+1:end,:);
x21lbd = x2lbd(1:N,:);
x22lbd = x2lbd(N+1:end,:);

Tw = h1*[zeros(1,N);[1/2*ones(N-1,1),tril(ones(N-1))-diag(1/2*ones(N-1,1))]];


phid            = Tw*x21d;
phird           = Tw*x21rd;
philbd          = Tw*x21lbd;

wd              = Tw*(x11d + phid);
wrd             = Tw*(x11rd + phird);
wlbd            = Tw*(x11lbd + philbd);


figure
set(gcf,'units','points','position',[x0screen,y0screen,WidthScreen,HeightScreen])
hold on
p1   = plot(zeta,wd(:,1),'LineWidth',lw);
p1r  = plot(zeta,wrd(:,1),'--','LineWidth',lw,'Color',Orange);
p1lb = plot(zeta,wlbd(:,1),'--','LineWidth',lw,'Color',Green);
set(gca,'FontSize',fst)
legSim{1} = sprintf('$w(\\zeta,t)$ PFEM');
legSim{2} = sprintf('$w(\\zeta,t)$ Preliminary ROM');
legSim{3} = sprintf('$w(\\zeta,t)$ FInal ROM');
legend(legSim,'Interpreter','latex','FontSize',fs,'Location','northwest')
title('Deformation','Interpreter','latex','FontSize',fs)
xlabel('space $(m)$','Interpreter','latex','FontSize',fs)
xlim([a,b])
% ylim([-ymax,ymax])
ylim([-10,10])
grid on

Nfigs = 500;
for j = 1:ceil(length(t)/Nfigs):length(t)

    set(p1,'YData',wd(:,j))
    set(p1r,'YData',wrd(:,j))
    set(p1lb,'YData',wlbd(:,j))

    pause(0.01)
end


