%This function returns a sparse matrix generated integrating the test functions (hat functions)
%over the interval [a,b] with N elements.
%D = Phi(b)*Phi(b)^\top

function D = InterconnectionMatrixRight(N)
D = sparse(N,N,1,N,N);
end