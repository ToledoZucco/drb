%This function returns a sparse matrix generated integrating the test functions (hat functions)
%over the interval [a,b] with N elements.
%E = int(Phi*Phi^\top)

function E = MassMatrix(a,b,N)

h = (b-a)/(N-1);

%% E matrix
id = (1:N)';
jd = (1:N)';
md = [h/3;2*h/3*ones(N-2,1);h/3];

isd = (1:N-1)';
jsd = (2:N)';
msd = h/6*ones(N-1,1);

iid = (2:N)';
jid = (1:N-1)';
mid = h/6*ones(N-1,1);

im = [id;isd;iid];
jm = [jd;jsd;jid];
mm = [md;msd;mid];

E = sparse(im,jm,mm,N,N);
end
